Freedesktop SDK sources are mirrored by
[lorry](https://gitlab.com/CodethinkLabs/lorry/lorry) and
[lorry-controller](https://gitlab.com/CodethinkLabs/lorry/lorry-controller). The
configuration for these mirrors lives here.

## Updating Mirror Configurations

Updating the lorry files with the latest changes to Freedesktop SDK requires
[bst-to-lorry](https://gitlab.com/CodethinkLabs/lorry/bst-to-lorry). The easiest
way of using this via `toolbox` using the container image we provide:

```
toolbox create freedesktop -i registry.gitlab.com/freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images/bst2:latest
toolbox enter freedesktop
```

Alternatively it can be installed in a python virtual environment:
```
python3 -m venv .venv
source ./.venv/bin/activate
pip install \
    git+https://gitlab.com/CodethinkLabs/lorry/bst-to-lorry \
    dulwich \
    packaging \
    requests \
    tomlkit
```

Then from a checkout of
[freedesktop-sdk](https://gitlab.com/freedesktop-sdk/freedesktop-sdk) at a
suitable branch (either `master` or a current `release/*` branch) run the
following (assuming a checkout of this repository exists at
`../mirroring-config`, relative to the `freedesktop-sdk` directory):

```
bst-to-lorry components.bst flatpak-release-repo.bst \
    --refspecs --git-directory ../mirroring-config/gits/ \
    --raw-files-directory ../mirroring-config/files/ \
    --exclude-alias fdsdk_git \
    --exclude-alias fdsdk_mirror
```

Then commit any changes and submit a merge request.
