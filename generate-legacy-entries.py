#!/usr/bin/env python3
"""Usage: python generate-legacy-entries.py [--directory DIRECTORY] [--output OUTPUT]"""

import argparse
import json
from pathlib import Path

import yaml


def main(directory, output_file):
    missing_shasums = []

    for file in Path(directory).glob("*.lorry"):
        if file.is_file():
            filename = file.name
            path = Path(directory).joinpath(filename)

            with open(path, "r", encoding="utf-8") as f:
                obj = yaml.safe_load(f)

            for name, data in obj.items():
                if name.endswith("/raw-files") and data["type"] == "raw-file":
                    for entry in data["urls"]:
                        if "sha256sum" not in entry:
                            destination = entry.get("destination", "")
                            missing_shasums.append(
                                {
                                    "filename": filename,
                                    "url": entry["url"],
                                    "destination": destination,
                                }
                            )

    missing_shasums.sort(key=lambda x: x["filename"])

    output_json = json.dumps(missing_shasums, indent=2)
    if missing_shasums:
        with open(output_file, "w", encoding="utf-8") as f:
            f.write(output_json)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Generate a json blob of legacy entries from lorry files without sha256sum"
    )
    parser.add_argument(
        "--directory",
        default="files",
        help="Directory with lorry files. Default: files",
    )
    parser.add_argument(
        "--output",
        default="shasums-ignore.json",
        help="Output file: Default: shasums-ignore.json",
    )
    args = parser.parse_args()

    main(args.directory, args.output)
