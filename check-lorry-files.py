#!/usr/bin/env python3

import json
import re
import sys
from pathlib import Path

from lorrycontroller import readconf

dir_path = Path(__file__).absolute().parent

subdirs = [d for d in dir_path.iterdir() if d.is_dir()]

dirs_to_ignore = [
    ".git",
]

legacy_file = "shasums-ignore.json"


def load_json_file(file):
    with open(file, "r", encoding="utf-8") as f:
        return json.load(f)


files_to_check = []
for subdir in subdirs:
    if subdir.name in dirs_to_ignore:
        continue

    lorry_files = [f for f in subdir.iterdir() if f.is_file() and f.suffix == ".lorry"]

    files_to_check.extend(lorry_files)

rc = readconf.ReadConfiguration({}, "")
error = False

legacy_entries = load_json_file(legacy_file)

for lorry_file in files_to_check:
    filepath = Path(dir_path, lorry_file)
    lorries = rc.get_valid_lorry_specs(filepath)

    # if parsed file is empty, there was probably an error
    if not lorries:
        error = True

    relative = filepath.relative_to(dir_path)

    for name, config in lorries:
        if config["type"] == "raw-file":
            for item in config.get("urls", []):
                url = item["url"]
                sha256sum = item.get("sha256sum")
                destination = item.get("destination", "")

                ignore_entry_found = any(
                    entry["filename"] == Path(lorry_file).name
                    and entry["url"] == url
                    and entry["destination"] == destination
                    for entry in legacy_entries
                )

                if sha256sum is not None and not re.match(
                    r"^[a-fA-F0-9]{64}$", sha256sum
                ):
                    error = True
                    print(
                        f"{name}: invalid sha256sum '{sha256sum}' for {url} in {relative}",
                        file=sys.stderr,
                    )

                if sha256sum is None and not ignore_entry_found:
                    error = True
                    print(
                        f"{name}: missing sha256sum for {url} in {relative}",
                        file=sys.stderr,
                    )

if error:
    sys.exit(1)
