#!/usr/bin/env python3

import glob

import yaml

for filename in glob.glob("gits/*.lorry"):
    with open(filename, encoding="utf-8") as f:
        data = yaml.safe_load(f)
    data = dict(sorted(data.items(), key=lambda item: (item[0], item[1]["url"])))
    with open(filename, "w", encoding="utf-8") as f:
        yaml.dump(data, f, sort_keys=False)

for filename in glob.glob("files/*.lorry"):
    with open(filename, encoding="utf-8") as f:
        data = yaml.safe_load(f)
    for lorry in data:
        urls = data[lorry]["urls"]
        data[lorry]["urls"] = sorted(
            urls, key=lambda d: (d["url"], str(d.get("destination", "")))
        )
    with open(filename, "w", encoding="utf-8") as f:
        yaml.dump(data, f, sort_keys=False)
